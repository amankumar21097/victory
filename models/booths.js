/* jshint indent: 1 */
const Sequelize = require('sequelize');

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('booths', {
		id: {
			type: DataTypes.INTEGER(11),
			allowNull: false,
			primaryKey: true,
			autoIncrement: true,
			field: 'id'
		},
		title: {
			type: DataTypes.STRING(11),
			allowNull: true,
			defaultValue:'',
		  },
		  description: {
			type: DataTypes.STRING(255),
			allowNull: false,
			field:'description'
		  },		  
		  status: {
			type: DataTypes.TINYINT(1),
			allowNull: true,
			defaultValue:1,
			field:'status'
		  },		  
		  createdAt: {
			type: DataTypes.DATE,
			allowNull: false,
			defaultValue: sequelize.literal('CURRENT_TIMESTAMP'),
			field: 'created_at'
		},
		updatedAt: {
			type: DataTypes.DATE,
			allowNull: false,
			defaultValue: sequelize.literal('CURRENT_TIMESTAMP'),
			field: 'updated_at'
		}
	}, {
		tableName: 'booths'
	});
};
