const bcrypt = require('bcrypt');
const db = require('../models');
let Validator = require('validatorjs');

const Booth = db.booths; 
const Sequelize = require('sequelize');

const Op = Sequelize.Op; 

module.exports = {
    create:async (req,res) => {

        if(req.session.aid){
           let message = "";
            res.render('booths/create',{
                title:'booth',
                message:''
                
            });
        }
        else{
            res.redirect('/login');
        }

    },

    index : (req ,res ) => {
        if(req.session.aid){

            Booth.findAll({
                order: [
                    ['id', 'DESC'],
                ],
            }).then(function(booths){
                res.render('booths/index',{
                    title: 'booths',
                    booths:booths,
                    flash:''
                });

            });
            
        }
        else{
            res.redirect('/login');
        }
           
    },
    
    show: (req,res ) => {
        
        if(req.session.aid){

            User.findOne({
                where:{
                    id:req.params.id
                }                
            }).then(function(user){
                res.render('users/view',{
                title:'users',
                message:'',
                user: user
            });
            });
        }
        else
        {
            res.redirect('/login');
        }   
    },
    
    store:(req,res) => {
        if(req.session.aid){

            let rules = {
                title: 'required',
                description: 'required', 
            }

            let validation  = new Validator(req.body, rules)
            if(validation.fails()){
                res.redirect('/booths/create');
            }

            Booth.create({
                title:req.body.title,
                description:req.body.description
            }).then(function () {
                res.redirect('/booths');
            })
        } else {
            res.redirect('/login');
        }
    },
    edit: (req,res) => {

        if(req.session.aid){
            Booth.findOne({
                where:{
                    id:req.params.id
                }
            }).then(function(booth){
                res.render('booths/edit',{
                    booth:booth,
                    title:'booths/edit',
                    message:''
                })
            })
        }
        else{
            res.redirect('/login');
        }

    },
    update: async(req,res) => {
        
        if(req.session.aid){

            let rules = {
                title: 'required',
                description: 'required', 
            }

            let validation  = new Validator(req.body, rules)
            if(validation.fails()){
                res.redirect('/booths/create');
            }

            await Booth.update({ 
                description:req.body.description,
                title:req.body.title
            },{
                where:{
                    id: req.body.booth_id
                }
            });
            req.flash('info','Booth Updated successfully')    
            res.redirect('/booths');           
        } 
        else
        {
            res.redirect('/login');
        } 
    },
    statusupdate: async(req,res) => {
        
        if(req.session.aid){

            

            await Booth.update({ 
                status:req.params.status
            },{
                where:{
                    id: req.params.id
                }
            });
            req.flash('info','Booth Updated successfully')    
            res.redirect('/booths');           
        } 
        else
        {
            res.redirect('/login');
        } 
    },
}

