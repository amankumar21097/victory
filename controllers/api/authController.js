const models = require('../../models'); 
const responseHelper = require('../../helpers/responseHelper');
const helperFunctions = require('../../helpers/helperFunctions');
const users = models.users;
const crypto = require('crypto')

var jwt = require('jsonwebtoken');
const { find } = require('lodash');
let secretKey = 'secret';

let salt = 'f844b09ff50c'

module.exports = {
    login: async (req, res) => {
        
        try {
            const required = {
                security_key: req.headers.security_key, 
                password: req.body.password, 
                phone: req.body.phone, 
            };
            const nonRequired = {  
                device_type: req.body.device_type,
                device_token: req.body.device_token,  
            };
            
            let requestData = await helperFunctions.vaildObject(required, nonRequired);
            let hashPassword = crypto.pbkdf2Sync(req.body.password, salt,  
                1000, 64, `sha512`).toString(`hex`);
            
            let checkUserExists = await users.findOne({
                where:{
                    phone:requestData.phone, 
                }
            })
            
            if(!checkUserExists){
                throw "User does not exist in our records" 
            }

            if(checkUserExists.password != hashPassword) {
                throw "Invalid password"
            }
            const payload = {
                id: checkUserExists.id,
                phone: checkUserExists.phone,  
            }
            var token = jwt.sign({
                data: payload
            }, secretKey);

            checkUserExists = checkUserExists.toJSON()
            delete checkUserExists.password  
            checkUserExists.token = token 
            res.status(200).json({
                'status': true,
                'code': 200,
                'message': 'User logged in successfully.',
                'body': checkUserExists

            });
        } catch (err) {
            return helperFunctions.error(res, err);
        }
    },
    logout: async (req, res) => {
        try {
            console.log(req.user,"----------hello");
            // return
            await users.update({
                deviceType: 0,
                deviceToken: '',
            },
                {
                    returning: true, where: { id: req.user.id }
                });
            req.session.destroy();
            res.status(200).json({
                'status': true,
                'code': 200,
                'message': 'User logged out successfully.',
                'body': {}
            });

        } catch (err) {
            console.log(err);
            return responseHelper.onError(res, err);
        }
    },
    getProfile: async (req, res) => {
        try {
            let getProfile = await users.findOne({
                where:{
                    id: req.user.id
                },
                raw:true
            })
            delete getProfile.password
            res.status(200).json({
                'status': true,
                'code': 200,
                'message': 'Data found successfully.',
                'body': getProfile
            });

        } catch (err) {
            console.log(err);
            return responseHelper.onError(res, err);
        }
    },
}