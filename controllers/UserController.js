const bcrypt = require('bcrypt');
const db = require('../models');
const User = db.users; 
const Booth = db.booths; 
const countries = db.countries; 
let Validator = require('validatorjs');
const Sequelize = require('sequelize');
const helperFunctions = require('../helpers/helperFunctions'); 

const Op = Sequelize.Op; 

module.exports = {
    create:async (req,res) => {

        if(req.session.aid){
           let message = "";
            const booths = await Booth.findAll({
                where:{
                    status:1
                },
            });
            // const getCountries =  await countries.findAll({});
            res.render('users/create',{
                title:'users',
                booths:booths,
                // countries: getCountries,
                message:''
                
            });
        }
        else{
            res.redirect('/login');
        }

    },

    index : (req ,res ) => {
        if(req.session.aid){

            User.findAll({
                where:{
                    id:{
                        [Op.ne]: req.session.aid,
                    },
                },
                order: [
                    ['id', 'DESC'],
                ],
            }).then(function(users){ 
                
                res.render('users/index',{
                    title: 'users',
                    users:users,
                    flash:''
                });

            });
            
        }
        else{
            res.redirect('/login');
        }
           
    },
    
    show: (req,res ) => {
        
        if(req.session.aid){

            User.findOne({
                where:{
                    id:req.params.id
                }                
            }).then(function(user){
                res.render('users/view',{
                title:'users',
                message:'',
                user: user
            });
            });
        }
        else
        {
            res.redirect('/login');
        }   
    },
    
    store: async (req,res) => {
        try {
            if(req.session.aid){
                let rules = {
                    booth_id: 'required',
                    name: 'required',  
                    color_wise: 'required',  
                    vid_number:'required',
                    caste:'required',
                    age:'required',
                    gender:'required',
                    phone:'required', 
                    location:'required',
                    relative_name:'required',
                    password:'required',
                    relations:'required',
                }
    
                
                let validation  = new Validator(req.body, rules)
                if(validation.fails()){ 
    
                    let booth_id = validation.errors.first('booth_id'); 
                    let name = validation.errors.first('name'); 
                    let color_wise = validation.errors.first('color_wise'); 
                    let vid_number = validation.errors.first('vid_number'); 
                    let caste = validation.errors.first('caste'); 
                    let age = validation.errors.first('age'); 
                    let gender = validation.errors.first('gender'); 
                    let phone = validation.errors.first('phone');  
                    let location = validation.errors.first('location'); 
                    let relations = validation.errors.first('relations'); 
                    let relative_name = validation.errors.first('relative_name'); 
                    let password = validation.errors.first('password'); 
    
                    let data = {
                        booth_id,
                        name,
                        color_wise,
                        vid_number,
                        caste,
                        age,
                        phone,
                        gender, 
                        location,
                        relations,
                        relative_name,
                        password,
                        status:406
                    }  
    
                    res.json(data); return
                } 
    
                req.body.password = await helperFunctions.bcryptHash(req.body.password);
                req.body.country_code = 91;
                const findUser = await User.findOne({
                    where:{
                        phone:req.body.phone,
                        country_code:req.body.country_code,
                    }
                })
                if(!findUser){
                    await User.create(req.body)     
                    res.json({messages : 'Added successfully', status:200 })
                     
                }else { 
                    res.json({messages : 'Phone number already exists', status:412 })
                    return
                }
            } else {
                res.redirect('/login');
            }
        } catch (error) {
            throw error
            console.log(error)
        }
        
    },
    checkphone: async (req,res) => {
        if(req.session.aid){

            const findUser = await User.findOne({
                where:{
                    phone:req.body.phone,
                    country_code:req.body.country_code,
                }
            })
            console.log(findUser,'------------const');
            let status = 0;
            if(findUser){
                let status = 0
            }else {
                status = 1;
            } 
            res.send({status:status}); 
        } else {
            res.redirect('/login');
        }
    }
}

