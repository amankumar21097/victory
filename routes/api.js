const passport = require("passport"); 

const authCtrl = require("../controllers/api/authController");

module.exports = function (app) {
     
    app.route("/api/login").put(authCtrl.login); 
    app.route("/api/logout").put(passport.authenticate("user", { session: false }), authCtrl.logout);
    app.route("/api/getProfile").get(passport.authenticate("user", { session: true }), authCtrl.getProfile);


}