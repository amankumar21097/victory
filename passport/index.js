const JWTStrategy = require('passport-jwt').Strategy;
const ExtractJWT = require('passport-jwt').ExtractJwt;
const db = require('../models'); 
const User = db.users; 
const opts = {};

opts.jwtFromRequest = ExtractJWT.fromAuthHeaderAsBearerToken();
opts.secretOrKey = 'secret';
module.exports = passport => {
	passport.use('user', new JWTStrategy(opts,
		async function (jwt_payload, done) {
			try {		
				console.log(jwt_payload,"-=====hello");

				const getUser = await User.findOne(
					{
						where: {
							id: jwt_payload.data.id,
							phone: jwt_payload.data.phone, 
						}
					});

				if (getUser) {
					return done(null, getUser.dataValues);
				}
				return done(null, false);
			} catch (e) {
				console.log('not local');
				console.error(e);
			}
		}
	));
}