/*
|----------------------------------------------------------------------------------------------------------------------------
|   Helpers File
|----------------------------------------------------------------------------------------------------------------------------
|
|   All helper methods in this file.
|
*/
const models = require('../models');
const sequelize = require('sequelize');
const { Op } = sequelize;
const bcrypt = require('bcrypt');
const crypto = require('crypto');
var path = require('path');
var uuid = require('uuid');
const fs = require('fs'); 
const nodemailer = require('nodemailer'); 
const countries = models.countries;
/*
|----------------------------------------------------------------------------------------------------------------------------
|   Exporting all methods
|----------------------------------------------------------------------------------------------------------------------------
*/
module.exports = {
    vaildObject: async function (required, non_required, res) {
        let message = '';
        let empty = [];

        let model = required.hasOwnProperty('model') && models.hasOwnProperty(required.model)
            ? models[required.model]
            : models.users;

        for (let key in required) {
            if (required.hasOwnProperty(key)) {
                if (required[key] == undefined || required[key] === '' && (required[key] !== '0' || required[key] !== 0)) {
                    empty.push(key);
                }
            }
        }


        if (empty.length != 0) {
            message = empty.toString();
            if (empty.length > 1) {
                message += " fields are required"
            } else {
                message += " field is required"
            }
            throw {
                'code': 400,
                'message': message
            }
        } else {
            if (required.hasOwnProperty('security_key')) {
                if (required.security_key != 'ElectionApp') {
                    message = "Invalid security key";
                    throw {
                        'code': 400,
                        'message': message
                    }
                }
            }

            if (required.hasOwnProperty('checkExists') && required.checkExists == 1) {
                const checkData = {
                    email: 'Email already exists, kindly use another.',
                    mobile: 'Mobile already exists, kindly use another',
                    phone: 'Phone number already exists, kindly use another',
                }

                for (let key in checkData) {
                    if (required.hasOwnProperty(key)) {
                        const checkExists = await model.findOne({
                            where: {
                                [key]: required[key].trim(),
                            },
                            raw: true,
                        });
                        if (checkExists) {
                             
                            if ((checkExists.hasOwnProperty('otpVerified') && checkExists.otpVerified == 1)) {
                                throw {
                                    code: 400,
                                    message: checkData[key]
                                }
                            
                            } else if((checkExists.hasOwnProperty('socialType') && checkExists.socialType != 0)){
                                await model.findOne({
                                    where: {
                                        [key]: required[key].trim(),
                                    },
                                    raw: true,
                                });

                            }else{

                                await module.exports.delete(model, checkExists.id); 
                            }
                        }
                    }
                }
            }

            const merge_object = Object.assign(required, non_required);
            delete merge_object.checkexit;
            delete merge_object.securitykey;

            if (merge_object.hasOwnProperty('password') && merge_object.password == '') {
                delete merge_object.password;
            }

            for (let data in merge_object) {
                if (merge_object[data] == undefined) {
                    delete merge_object[data];
                } else {
                    if (typeof merge_object[data] == 'string') {
                        merge_object[data] = merge_object[data].trim();
                    }
                }
            }

            return merge_object;
        }
    },
    save: async (model, data, returnData = false, req = {}) => {
        console.log(req,"============reqfdd");
        try {
            if (!(typeof data == 'object' && !Array.isArray(data))) {
                throw 'Please send valid object in second argument of save function.';
            }
            // console.log(model, '===================>model');
            const tableColumns = model.rawAttributes
            console.log(tableColumns, '==============>tableColumns');
            const defaultValues = {
                'INTEGER': 0,
                'STRING': '',
                'TEXT': '',
                'FLOAT': 0,
                'DECIMAL': 0,
            }

            data = { ...data };
            let rawData = { ...data };

            for (let key in data) {
                if (!tableColumns.hasOwnProperty(key)) {
                    delete data[key];
                } else {
                    const tableColumn = tableColumns[key];
                    const tableDataType = tableColumn.type.key;
                    if (!data[key] && !tableColumn.hasOwnProperty('defaultValue')) {
                        data[key] = defaultValues[tableDataType]
                    }
                }
            }

            for (let column in tableColumns) {
                if (column != 'createdAt' && column != 'updatedAt' && column != 'id' && !data.hasOwnProperty('id')) {
                    const tableColumn = tableColumns[column];
                    const tableDataType = tableColumn.type.key;


                    if (!data.hasOwnProperty(column)) {
                        if (!tableColumn.hasOwnProperty('defaultValue')) {
                            data[column] = defaultValues[tableDataType];
                        } else {
                            data[column] = tableColumn.defaultValue;
                        }
                    }
                }
            }

            let id; 

            if (data.hasOwnProperty('id')) {
                const updatedEntry = await model.update(
                    data,
                    {
                        where: {
                            id: data.id,
                        },
                        individualHooks: true
                    }
                );
                id = data.id;
            } else {
                const createdEntry = await model.create(data);
                id = createdEntry.dataValues.id;
            }

            if (returnData) {
                console.log(id, '======================+>id');

                let getData = await model.findOne({
                    where: {
                        id
                    }
                });
                getData = getData.toJSON();
                if (getData.hasOwnProperty('password')) {
                    delete getData['password'];
                }

                if (rawData.hasOwnProperty('imageFolders') && typeof rawData.imageFolders == 'object' && !Array.isArray(rawData.imageFolders) && Object.keys(rawData.imageFolders).length > 0 && Object.keys(req).length > 0) {
                    for (let column in rawData.imageFolders) {
                        const folder = rawData.imageFolders[column];
                        if (getData.hasOwnProperty(column) && getData[column] != '') {
                            if (!getData[column].includes('http')) {
                                getData[column] = `${req.protocol}://${req.get('host')}/images/${folder}/${getData[column]}`
                            }
                            // getData[column] = `${req.protocol}://${req.get('host')}/api/get/${getData[column]}`
                        }
                    }
                }

                return getData;
            }

            return id;
        } catch (err) {
            throw err;
        }
    },
    delete: async (model, id) => {
        await model.destroy({
            where: {
                id: {
                    [sequelize.Op.in]: Array.isArray(id) ? id : [id]
                }
            }
        });
    },

    isJson(item) {
        item = typeof item !== "string" ? JSON.stringify(item) : item;

        try {
            item = JSON.parse(item);
        } catch (e) {
            return false;
        }

        if (typeof item === "object" && item !== null) {
            return true;
        }

        return false;
    },

    clone: function (value) {
        return JSON.parse(JSON.stringify(value));
    },


    time: function () {
        var time = Date.now();
        var n = time / 1000;
        return time = Math.floor(n);
    },
    getCountries: async function(){
        return await countries.findAll({});
    },
    generateOTP: () => Math.floor(1000 + Math.random() * 9000),
    success: function (res, message = '', body = {}) {
        return res.status(200).json({
            'success': true,
            'code': 200,
            'message': message,
            'body': body
        });
    },

    error: function (res, err, req) {
        let code = (typeof err === 'object') ? (err.code) ? err.code : 403 : 403;
        let message = (typeof err === 'object') ? (err.message ? err.message : '') : err;
        if (req) {
            req.flash('flashMessage', { color: 'error', message });

            const originalUrl = req.originalUrl.split('/')[1];
            return res.redirect(`/${originalUrl}`);
        }

        return res.status(code).json({
            'success': false,
            'message': message,
            'code': code,
            'body': {}
        });

    },
    bcryptHash: (myPlaintextPassword, saltRounds = 10) => {
        const bcrypt = require('bcrypt');
        const salt = bcrypt.genSaltSync(saltRounds);
        let hash = bcrypt.hashSync(myPlaintextPassword, salt);
        hash = hash.replace('$2b$', '$2y$');
        return hash;
    },

    comparePass: async (requestPass, dbPass) => {
        dbPass = dbPass.replace('$2y$', '$2b$');
        const match = await bcrypt.compare(requestPass, dbPass);
        return match;
    },
    createSHA1: function () {
        let key = 'abc' + new Date().getTime();
        return crypto.createHash('sha1').update(key).digest('hex');
    },
  
    imageUpload: (file, folder = 'users') => {
        if (file.name == '') return;

        let file_name_string = file.name;
        var file_name_array = file_name_string.split(".");
        var file_extension = file_name_array[file_name_array.length - 1];
        var result = "";
        result = uuid();
        let name = result + '.' + file_extension;
         
        file.mv(`images${folder ? `/${folder}` : ''}/${name}`, function (err) {
            if (err) throw err;
        });
        return name;
    },
    imageUploadWithRealName: (file, folder = 'users') => {
        if (file.name == '') return;

        let file_name_string = file.name;
        var file_name_array = file_name_string.split(".");
        var file_extension = file_name_array[file_name_array.length - 1];
        var letters = "ABCDE1234567890FGHJK1234567890MNPQRSTUXY";
        var result = "";
         
        file.mv('public/uploads/' + folder + '/' + file_name_string, function (err) {
            if (err) throw err;
        });
        return file_name_string;
    },

    uploadImage: function (fileName, file, folderPath) {
        const rootPath = path.join(path.resolve(__dirname), '../');
        const imageBuffer = decodeBase64Image(file);
        const newPath = `${rootPath}${folderPath}${fileName}`;
        writeDataStream(newPath, imageBuffer.data);
        return newPath;
    },

    fileUpload(file, folder = 'users') {
        let file_name_string = file.name;
        var file_name_array = file_name_string.split(".");
        var file_extension = file_name_array[file_name_array.length - 1];
        var letters = "ABCDE1234567890FGHJK1234567890MNPQRSTUXY";
        var result = "";
        while (result.length < 28) {
            var rand_int = Math.floor((Math.random() * 19) + 1);
            var rand_chr = letters[rand_int];
            if (result.substr(-1, 1) != rand_chr) result += rand_chr;
        }
        let name = result + '.' + file_extension; 
        file.mv('public/images/' + folder + '/' + name, function (err) {
            if (err) {
                throw err;
            }
        });
        return name;
    },

    isValidDate(d) {
        return d instanceof Date && !isNaN(d);
    }


}